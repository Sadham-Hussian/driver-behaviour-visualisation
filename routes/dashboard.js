var express = require('express');
var router = express.Router();

/* GET realtime page. */
router.get('/', function(req, res) {

	res.render('dashboard', { title: 'Visualisation of driver behaviour iot application' });
});


module.exports = router;